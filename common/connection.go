package common

//package nxnet

import (
	"bufio"
	"log"
	"net"

	bp "github.com/nexustix/boilerplate"
	"gitlab.com/nxgamestack/nxframe/nxframe-go/nxwrap"
)

type Connection struct {
	conn        net.Conn
	alive       bool
	started     bool
	receivechan *chan nxwrap.Message
	sendchan    chan []byte
	tsize       int
	rw          *bufio.ReadWriter
	id          uint
	programID   string
}

func (c *Connection) setup() {
	r := bufio.NewReader(c.conn)
	w := bufio.NewWriter(c.conn)
	c.rw = bufio.NewReadWriter(r, w)
	c.onConnect()

}

func (c *Connection) onConnect() {
	// FIXME send on first message and not on connect ?
	// (prevent spam on TLS servers)
	*c.receivechan <- nxwrap.Message{SourceID: c.programID, Kind: nxwrap.KindJoin, ChannelID: c.id, Data: []byte("")}
}

func (c *Connection) Disconnect() {
	if c.alive {
		c.conn.Close()
		*c.receivechan <- nxwrap.Message{SourceID: c.programID, Kind: nxwrap.KindLeave, ChannelID: c.id, Data: []byte("")}
	}
	c.alive = false
}

func NewConnection(conn net.Conn, id uint, receivechan *chan nxwrap.Message, sourceID string) *Connection {
	connection := &Connection{
		conn:  conn,
		alive: true,
		//inchan:  make(chan []byte, 8),
		receivechan: receivechan,
		sendchan:    make(chan []byte, 8),
		tsize:       1024,
		id:          id,
		started:     false,
		programID:   sourceID,
	}
	connection.setup()
	return connection
}

func (c *Connection) handleIncomming() {
	for {
		buff := make([]byte, c.tsize)
		n, err := c.rw.Read(buff)
		if bp.GotError(err) {
			log.Printf("<-> INFO fail socket reading >%s<", err)
			//c.alive = false
			break
		} else {
			*c.receivechan <- nxwrap.Message{SourceID: c.programID, Kind: nxwrap.KindData, ChannelID: c.id, Data: buff[0:n]}
		}
	}
	c.Disconnect()
}

func (c *Connection) handleOutgoing() {
	for c.alive {
		select {
		case buff := <-c.sendchan:
			_, err := c.rw.Write(buff)
			//HACK ?
			c.rw.Flush()
			if bp.GotError(err) {
				log.Printf("<!> INFO fail socket sending >%s<", err)
				//c.alive = false
				break
			} else {
				c.rw.Flush()
			}
		}

	}
	c.Disconnect()
}

func (c *Connection) Start() {
	if !c.started {
		go c.handleIncomming()
		go c.handleOutgoing()
		c.started = true
	}
}

func (c *Connection) IsAlive() bool {
	return c.alive
}

func (c *Connection) SendMessage(msg nxwrap.Message) {
	if c.alive {
		c.sendchan <- msg.Data
	}
}
