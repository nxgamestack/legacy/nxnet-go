package tcpBasic

import (
	"fmt"
	"log"
	"net"

	"gitlab.com/nxgamestack/nxframe/nxframe-go/nxwrap"
	"gitlab.com/nxgamestack/nxnet/nxnet-go/common"
)

type Server struct {
	hostname       string
	port           string
	SourceID       string
	idCounter      uint
	connections    map[uint]*common.Connection
	receiveChannel chan nxwrap.Message
	sendChannel    chan nxwrap.Message
}

func NewServer(hostname string, port string, sourceID string) *Server {
	if len(sourceID) < 1 {
		panic("SourceID needs to have 1 character or more")
	}
	server := &Server{
		hostname:       hostname,
		port:           port,
		idCounter:      1,
		connections:    make(map[uint]*common.Connection),
		receiveChannel: make(chan nxwrap.Message, 8),
		sendChannel:    make(chan nxwrap.Message, 8),
		SourceID:       sourceID,
	}
	return server
}

func (s *Server) cleanUp() {
	for k, v := range s.connections {
		if !v.IsAlive() {
			defer delete(s.connections, k)
		}
	}
}

func (s *Server) GetReceiveChannel() chan nxwrap.Message {
	return s.receiveChannel
}

func (s *Server) GetSendChannel() chan nxwrap.Message {
	return s.sendChannel
}

func (s *Server) Listen() {
	var listener net.Listener
	var err error

	listener, err = net.Listen("tcp", fmt.Sprintf("%s:%s", s.hostname, s.port))
	if err != nil {
		log.Fatal("<!> ERR starting tcp server")
	}
	defer listener.Close()

	for {
		conn, _ := listener.Accept()
		if err != nil {
			log.Printf("<-> WARN fail accepting: %s\n", err)
		} else {
			connection := common.NewConnection(conn, s.idCounter, &s.receiveChannel, s.SourceID)
			s.connections[s.idCounter] = connection
			s.connections[s.idCounter].Start()
			s.idCounter = s.idCounter + 1
		}
		s.cleanUp()
	}
}

func (s *Server) Send() {
	for {
		select {
		case msg := <-s.sendChannel:
			if msg.ChannelID == 0 {
				switch msg.Kind {
				case nxwrap.KindData:
					for _, v := range s.connections {
						v.SendMessage(msg)
					}
				}

			} else {
				// TODO merge if statements ?
				if val, ok := s.connections[msg.ChannelID]; ok {
					if val.IsAlive() {
						switch msg.Kind {
						case nxwrap.KindData:
							val.SendMessage(msg)
						}
					} else {
						log.Printf("<!> WARN trying to send to dead connection: %v \n", msg.ChannelID)
					}
				} else {
					log.Printf("<!> WARN trying to send to invalid connection: %v \n", msg.ChannelID)
				}
			}
		}
	}
}
